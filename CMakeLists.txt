cmake_minimum_required(VERSION 3.22)
project(ogs6py)

message("please add '${PROJECT_BINARY_DIR}/ThirdParty/ogs/lib' to PYTHONPATH environment variable")
message("export PYTHONPATH=$PYTHONPATH:${PROJECT_BINARY_DIR}/ThirdParty/ogs/lib")

add_subdirectory(ThirdParty/ogs)


