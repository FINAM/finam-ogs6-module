from datetime import datetime, timedelta
import numpy as np

import finam as fm

from ogs import mesh
from ogs import simulator
import tempfile

from dataclasses import dataclass


@dataclass
class CouplingInfo:
    mesh_name: str
    variable_name: str
    data_location: str  # ToDo: make it later on a finam data_location type
    unit: str


def getKey(coupling_info):
    return (
        coupling_info.mesh_name
        + "_"
        + coupling_info.variable_name
        + "_"
        + coupling_info.data_location
    )


def getDataArrayFromMesh(coupling_info, simulator):
    # get the OGS mesh according to the grid name
    ogs_coupling_grid = simulator.getMesh(coupling_info.mesh_name)
    if coupling_info.data_location == "POINTS":
        return ogs_coupling_grid.getPointDataArray(coupling_info.variable_name, 1)
    return ogs_coupling_grid.getCellDataArray(coupling_info.variable_name, 1)


def vtk2esmf_cell_types(vtk_cell_types):
    esmf_cell_types = []
    for vtk_cell_type in vtk_cell_types:
        if vtk_cell_type == 5:  # vtk triangle
            esmf_cell_types.append(int(fm.CellType.TRI))
        if vtk_cell_type == 9:  # vtk quad
            esmf_cell_types.append(int(fm.CellType.QUAD))
    return esmf_cell_types


def getGrid(coupling_info, simulator):
    grid_name = coupling_info.mesh_name
    # get the OGS mesh according to the grid name
    ogs_coupling_grid = simulator.getMesh(grid_name)
    # create points, cells, and cell_types for the finam.UnstructuredGrid
    coordinates = np.array(ogs_coupling_grid.getPointCoordinates())
    number_of_coordinates = len(coordinates)
    number_of_points = int(number_of_coordinates / 3)
    coordinates.shape = (number_of_points, 3)
    coordinates_2d = coordinates[:, [0, 1]]

    cell_id_list, vtk_cell_types = ogs_coupling_grid.getCells()
    # FINAM cell_types follow ESMF cell type convention
    # (https://earthsystemmodeling.org/docs/release/latest/ESMF_refdoc/node5.html#const:meshelemtype)
    # ESMF conventions differs from vtk cell type convention
    # (https://examples.vtk.org/site/VTKFileFormats/#legacy-file-examples
    # and https://examples.vtk.org/site/VTKBook/05Chapter5/#54-cell-types)
    cell_types = vtk2esmf_cell_types(vtk_cell_types)
    cell_ids = np.asarray(cell_id_list, dtype=int)
    cell_ids.shape = (len(cell_types), int(len(cell_ids) / len(cell_types)))

    finam_coupling_grid = fm.UnstructuredGrid(
        points=coordinates_2d,
        cells=cell_ids,
        cell_types=cell_types,
        order="C",
        data_location=coupling_info.data_location,
    )

    return finam_coupling_grid


class OGS6Model(fm.TimeComponent):
    def __init__(
        self,
        start,
        step,
        project_file,
        output_dir,
        input_coupling_info,
        output_coupling_info,
    ):
        super(OGS6Model, self).__init__()
        if not isinstance(start, datetime):
            raise ValueError("Start must be of type datetime")
        if not isinstance(step, timedelta):
            raise ValueError("Step must be of type timedelta")
        self.time = start
        if not isinstance(step, timedelta):
            raise ValueError("Step is not a timedelta.")
        self.step = step
        self.project_file = project_file
        self.output_dir = output_dir
        self.input_coupling_info = input_coupling_info
        self.output_coupling_info = output_coupling_info

    @property
    def next_time(self):
        return self.time + self.step

    def _initialize(self):
        arguments = ["", self.project_file, "-l debug", "-o " + self.output_dir]

        self.logger.info("initialize OGS ...")
        simulator.initialize(arguments)
        self.logger.info("initialize OGS done")

        self.logger.info("initialize outputs ...")
        for oci in self.output_coupling_info:
            self.logger.info("initialize " + str(oci.mesh_name) + " for output")
            self.logger.info("- data array " + str(oci.variable_name))
            self.logger.info("- data_location " + str(oci.data_location))
            self.logger.info("- unit " + str(oci.unit))
            finam_coupling_grid = getGrid(coupling_info=oci, simulator=simulator)
            self.outputs.add(
                name=getKey(oci),
                time=self.time,
                grid=finam_coupling_grid,
                unit=oci.unit,
            )

        for ici in self.input_coupling_info:
            self.logger.info(
                "initialize "
                + str(ici.mesh_name)
                + " (variable is "
                + ici.variable_name
                + ") with unit "
                + ici.unit
                + " for input"
            )
            finam_coupling_grid = getGrid(coupling_info=ici, simulator=simulator)
            self.inputs.add(
                name=ici.mesh_name,
                time=self.time,
                grid=finam_coupling_grid,
                units=ici.unit,
            )

        self.logger.info("initialize create_connector ...")
        self.create_connector()
        self.logger.info("initialize create_connector done")

    def _connect(self, start_time):
        self.logger.info("connect() ...")
        push_data = {
            getKey(oci): getDataArrayFromMesh(oci, simulator)
            for oci in self.output_coupling_info
        }
        self.try_connect(start_time, push_data=push_data)
        self.logger.info("connect() done")

    def _validate(self):
        pass

    def _update(self):
        self.logger.info("update OGS ...")
        self.logger.info("update time ...")
        self.time += self.step
        self.logger.info("... updated time")

        self.logger.info("read input boundary condition values")

        for ici in self.input_coupling_info:
            self.logger.info(self._inputs[ici.mesh_name])
            bc_data = self._inputs[ici.mesh_name].pull_data(self.time).magnitude
            ogs_mesh = simulator.getMesh(ici.mesh_name)
            cells, celltypes = ogs_mesh.getCells()
            bc_values = np.array(bc_data).reshape(len(celltypes))
            ogs_mesh.setCellDataArray(ici.variable_name, bc_values, 1)

        self.logger.info("read input boundary condition values done")

        self.logger.info("simulator.executeTimeStep() ...")
        simulator.executeTimeStep()
        self.logger.info("simulator.executeTimeStep() done")
        self.logger.info("update OGS done")

        for oci in self.output_coupling_info:
            self.logger.info(self._outputs[getKey(oci)])
            ogs_mesh = simulator.getMesh(oci.mesh_name)
            data = ogs_mesh.getPointDataArray(oci.variable_name, 1)  # head is on points
            self._outputs[getKey(oci)].push_data(time=self.time, data=data)

    def _finalize(self):
        simulator.finalize()
