# FINAM OGS6 Module

The module is based on the FINAM coupler and the OGS6 python interface. The
FINAM coupler defines the interface and provides the infrastructure for the
coupling. It wraps the [OpenGeoSys](https://www.opengeosys.org) simulation
software.

## License

For open source projects, say how it is licensed.

## Project status

Work in progress. FINAM OGS6 module can be coupled to other modules.

## Installation

All necessary python modules will be installed into a python virtual
environment.

### Clone the sources for the finam ogs6 module

```bash
git clone git@git.ufz.de:FINAM/finam-ogs6-module.git
cd finam-ogs6-module
```

### Create python virtual environment

```bash
python -m venv .venv
source .venv/bin/activate
```

### Install the FINAM package

```bash
pip install git+https://git.ufz.de/FINAM/finam.git
```

### Install the OGS Python Interface

```bash
pip install ogs
```
or development version:
```bash
cd ogs-source-dir
source scripts/env/eve/cli.sh
CMAKE_ARGS="-DOGS_BUILD_PROCESSES=ComponentTransport;HT;HeatConduction;HydroMechanics;LiquidFlow;LIE;ThermoRichardsMechanics;RichardsComponentTransport;RichardsFlow;RichardsMechanics;SmallDeformation;SmallDeformationNonlocal;SteadyStateDiffusion;TES;TH2M;ThermalTwoPhaseFlowWithPP;ThermoHydroMechanics;ThermoMechanicalPhaseField;ThermoMechanics;ThermoRichardsFlow;TwoPhaseFlowWithPP;TwoPhaseFlowWithPrho -DOGS_USE_MFRONT=OFF" pip install -v .\[test\]
```

### Testing OGS Python Interface

Now, in order to check if the installation was successful it is possible to run
the OGS python interface tests:
```bash
pytest
```

### Install the FINAM OGS6 module into the python virtual environment

In the folder of the FINAM OGS6 module:
```bash
pip install .
```

# Test the FINAM OGS6 Module

```bash
pytest tests/test_ogs6_model.py
```
