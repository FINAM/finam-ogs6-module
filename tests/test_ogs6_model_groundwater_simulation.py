import unittest
from datetime import datetime, timedelta

import tempfile
from finam_ogs6_module.ogs6_model import OGS6Model, CouplingInfo


class TestOGS6Model(unittest.TestCase):
    def test_OGS6_model(self):
        start_time = datetime(2000, 1, 1)
        project_file = "/home/fischeth/w/o/s/Tests/Data/Parabolic/LiquidFlow/Flux/3D/Hex/cuboid_1x1x1_hex_27_Dirichlet_Dirichlet_Python.prj"
        output_dir = tempfile.mkdtemp()
        finam_coupling_grids = []  #'cuboid_1x1x1_hex_27_top_boundary']

        model = OGS6Model(
            start_time,
            step=timedelta(days=7),
            project_file=project_file,
            output_dir=output_dir,
            output_coupling_info=finam_coupling_grids,
            input_coupling_info=finam_coupling_grids,
        )
        self.assertTrue(isinstance(model, OGS6Model))
        self.assertEqual(model.time, start_time)
        self.assertEqual(model.step, timedelta(days=7))

        model.initialize()
        print("test_OGS6_model initialize done")
        # self.assertEqual(len(model.inputs), 2)
        self.assertEqual(len(model.outputs), len(finam_coupling_grids))

        print("test_OGS6_model model.connect() ...")
        model.connect(start_time)
        print("test_OGS6_model model.connect() done")
        # self.assertEqual(model.outputs["Sum"].get_data(model.time), 0)

        model.validate()
        print("test_OGS6_model model.validate() done")

        print("model.update() ...")
        model.update()
        print("model.update() done")

        model.finalize()

    # def test_coupled(self):
    #    from finam.modules.generators import CallbackGenerator

    #    source = CallbackGenerator(
    #        {
    #            "Value_A": lambda t: 5,
    #            "Value_B": lambda t: 1,
    #        },
    #        start=datetime(2000, 1, 1),
    #        step=timedelta(days=1))

    #    source.initialize()

    #    model = OGS6Model(start=datetime(2000, 1, 1), step=timedelta(days=1))
    #    model.initialize()

    #    source.outputs["Value_A"] >> model.inputs["A"]
    #    source.outputs["Value_B"] >> model.inputs["B"]

    #    source.connect()
    #    model.connect()

    #    source.validate()
    #    model.validate()

    #    model.update()
    #    source.update()

    #    self.assertEqual(model.status, ComponentStatus.UPDATED)
    #    self.assertEqual(model.time, datetime(2000, 1, 2))
    #    self.assertEqual(model.outputs["Sum"].get_data(model.time), 5 + 1)

    #    model.finalize()
    #    self.assertEqual(model.status, ComponentStatus.FINALIZED)
