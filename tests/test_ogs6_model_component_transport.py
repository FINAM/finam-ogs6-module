import unittest
from datetime import datetime, timedelta

import tempfile
from finam_ogs6_module.ogs6_model import OGS6Model, CouplingInfo


class TestOGS6Model(unittest.TestCase):
    def test_OGS6_model(self):
        start_time = datetime(2000, 1, 1)
        project_file = "/data/landtrans/landtech/models/bodium-ogs-coupling/BodiumOGS/simulation_modell/AdvectionDiffusionSorptionDecay.prj"
        output_dir = tempfile.mkdtemp()
        finam_output_coupling_grids = []
        finam_input_coupling_grids = []

        model = OGS6Model(
            start_time,
            step=timedelta(days=7),
            project_file=project_file,
            output_dir=output_dir,
            output_coupling_info=finam_output_coupling_grids,
            input_coupling_info=finam_input_coupling_grids,
        )
        self.assertTrue(isinstance(model, OGS6Model))
        self.assertEqual(model.time, start_time)
        self.assertEqual(model.step, timedelta(days=7))

        model.initialize()
        print("test_OGS6_model initialize done")
        # self.assertEqual(len(model.inputs), 2)
        self.assertEqual(len(model.outputs), len(finam_output_coupling_grids))

        print("test_OGS6_model model.connect() ...")
        model.connect(start_time)
        print("test_OGS6_model model.connect() done")
        # self.assertEqual(model.outputs["Sum"].get_data(model.time), 0)

        model.validate()
        print("test_OGS6_model model.validate() done")

        print("model.update() ...")
        model.update()
        print("model.update() done")

        model.finalize()
